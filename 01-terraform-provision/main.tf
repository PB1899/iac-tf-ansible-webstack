provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

data "aws_availability_zones" "AZs" {
  state = "available"
}

#GET RHEL 8.2 ami
data "aws_ami" "rhel8" {
  owners      = ["309956199498"]
  most_recent = true

  filter {
    name   = "name"
    values = ["RHEL-8.2*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

locals {
  service_name = "webstack"
  project      = "IaC Ansible-TF"
}

locals {
  common_tags = {
    Service = local.service_name
    Owner   = local.project
  }
}

resource "aws_key_pair" "masterkey" {
  key_name   = "master"
  public_key = file("~/.ssh/rhel-8-master.pub")
}

resource "aws_vpc" "webstack_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = merge(
    {
      Name = format("vpc-%s", local.service_name)
    },
    local.common_tags
  )
}


resource "aws_internet_gateway" "webstack_igw" {
  vpc_id = aws_vpc.webstack_vpc.id
  tags   = local.common_tags
}

resource "aws_route_table" "webstack" {
  vpc_id = aws_vpc.webstack_vpc.id
  tags   = local.common_tags
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.webstack.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.webstack_igw.id
  timeouts {
    create = "5m"
  }
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.webstack_subnet.id
  route_table_id = aws_route_table.webstack.id
}

resource "aws_subnet" "webstack_subnet" {
  cidr_block        = "10.0.1.0/24"
  vpc_id            = aws_vpc.webstack_vpc.id
  availability_zone = data.aws_availability_zones.AZs.names[0]
  tags              = local.common_tags
}

resource "aws_security_group" "webstack" {
  vpc_id = aws_vpc.webstack_vpc.id
  tags   = local.common_tags
}

resource "aws_security_group_rule" "ssh" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.webstack.id
  to_port           = 22
  type              = "ingress"
  cidr_blocks       = [var.external_ip]
}

resource "aws_security_group_rule" "http_inbound" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.webstack.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "outbound" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.webstack.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}



resource "aws_instance" "lb_server" {
  ami                         = data.aws_ami.rhel8.id
  instance_type               = "t2.small"
  subnet_id                   = aws_subnet.webstack_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.webstack.id]
  key_name                    = aws_key_pair.masterkey.key_name
  depends_on                  = [aws_internet_gateway.webstack_igw]
  tags = merge(
    local.common_tags,
    {
      Name  = "loadbalancer"
      Group = "loadbalancers"
    },
  )
}

resource "aws_instance" "webserver" {
  ami                         = data.aws_ami.rhel8.id
  instance_type               = "t2.micro"
  count                       = var.instance_count
  subnet_id                   = aws_subnet.webstack_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.webstack.id]
  key_name                    = aws_key_pair.masterkey.key_name
  tags = merge(
    local.common_tags,
    {
      Name  = "webserver-${count.index + 1}"
      Group = "webservers"
    },
  )
}