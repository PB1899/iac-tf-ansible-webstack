variable "aws_profile" {
  description = "your AWS profile"
  type        = string
}

variable "aws_region" {
  description = "AWS region to deploy to"
  type        = string
}

variable "external_ip" {
  description = "the IP addresses you're allowed to SSH into the machine"
  type        = string
  //  default     = "0.0.0.0/0"
}

variable "instance_count" {
  default = "2"
}
