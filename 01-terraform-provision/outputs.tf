output "rhel_8_ami" {
  value = data.aws_ami.rhel8.id
}

output "loadbalancer_dns" {
  value = aws_instance.lb_server.public_dns
}

output "loadbalancer_ip" {
  value = aws_instance.lb_server.public_ip
}

output "webserver_ip" {
  value = aws_instance.webserver[*].public_ip
}