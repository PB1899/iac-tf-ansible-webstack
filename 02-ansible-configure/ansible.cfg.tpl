# ansible.cfg

[defaults]
inventory = ./aws_ec2.yml
remote_user = ec2-user
private_key_file = /path/to/your/key
enable_plugins = aws_ec2
host_key_checking = False
retry_files_enabled = False
nocows = True
