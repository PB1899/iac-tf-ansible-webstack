#!/bin/bash

# get current URL (ISP keeps changing):
export TF_VAR_external_ip="$(curl -s ifconfig.me)/32"

echo -e "My IP: $TF_VAR_external_ip"

cd 01-terraform-provision

echo "Formatting the files"
terraform fmt

VAL=$(terraform validate -no-color)
if [[ $VAL == "Success! The configuration is valid." ]]
then
  # show the plan (comment out when the config is ready):
  terraform  apply --auto-approve
else
  echo "!!!!!!!!!!!!!!!!!!!!!! Error !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo $VAL
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  exit 1
fi

sleep 180

cd ../02-ansible-configure
export AWS_PROFILE=$(grep  profile ../01-terraform-provision/terraform.tfvars | awk {'print $3'} | tr -d '"')
ansible-playbook main.yml

sleep 60

echo "Testing the web stack:"
cd ../01-terraform-provision
curl -I http://$(terraform output -raw loadbalancer_dns)
