# Provision and configure a web stack with Terraform and Ansible

This project is provisioning a two-tier system with two web servers and a load balancer instance on AWS using Terraform. The servers are configured using Ansible to run Apache as the webservice as well as the load balancer.

### Prerequisites:
- an existing AWS account
- Terraform installed
- Ansible installed
- boto3 installed

### Details

The resources provisioned:
- VPC (with a public subnet)
- Security group with rules to allow incoming SSH and HTTP connection
- Route table
- SSH key pair
- 3 EC2 instances

The [original project](https://github.com/ACloudGuru-Resources/Course_Introduction_to_Ansible) used CloudFormation for provisioning and Amazon Linux images in the default VPC (see the original CF file in the cloudformation directory). I made several changes, such as:
- Terraform for provisioning
- provision a separate VPC and all related networking components (subnet, security group and rules, route table)
- make use of the [aws_ec2 Ansible plugin](https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html) for dynamic inventory
- using RHEL 8.2 images on AWS (hence the data_source block in the configuration)
- using firewalld as the firewall service - that's not installed by default on AWS
- building on RHEL also needs changes in SELinux so that the load balancer can communicate to the web servers, so there's an additional block in the Ansible configuration for this (analogous to running
``` setsebool -P httpd_can_network_connect 1``` as root)

### How to build the configuration

Run the Bash script that wraps the Terraform provisioning and the Ansible configuration steps together, this also takes care of the setting of the necessary variables:
```bash
$ ./apply-config.sh
```

### Clean up
When finished testing, or the webstack is no longer needed, run:
```bash
$ cd 01-terraform-provision
$ terraform destroy --auto-approve
```
